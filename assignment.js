// put your javascript code here
var animalPictures = function() {
    var animal_pictures = $('#animal-pictures').html();
    var compile_animal_pictures = Handlebars.compile(animal_pictures);
    var compiled_html = compile_animal_pictures(animals_data);
    $('.container').html(compiled_html);
};

var animalCategories = function() {
    var animal_categories = $('#animal-categories').html();
    var compiled_animal_categories = Handlebars.compile(animal_categories);
    var compiled_html = compiled_animal_categories(animals_data);
    $('.container').html(compiled_html);
};

var showAnimalDetails = function(input) {
    var animal_details = $('#animal-details').html();
    var compiled_animal_details = Handlebars.compile(animal_details);
    var compiled_html = compiled_animal_details(input);
    $('.container').html(compiled_html);
};

var showAnimalCategory = function(input) {
    var animal_details = $('#single-animal-category').html();
    var compiled_animal_category = Handlebars.compile(animal_details);
    var compiled_html = compiled_animal_category(input);
    $('.container').html(compiled_html);
}

var animalDetails = function() {
    var current_category = animals_data.category[0];
    var current_animal = current_category.animals[0];
    
    $('.category-call').on('click', function() {
        var index = $(this).data('id');
        current_category = category[index];
        showAnimalDetails(current_category);
    });
};
//showAnimalDetails(animals_data.category[1].animals[0]);

(function() {
    animalCategories();
    var navs = $('nav');
    var className = '';
    navs.on('click', 'li', function() {
        $(this)
            .addClass('active')
            .siblings('li')
                .removeClass('active');
        className = $(this).text().toLowerCase();
        if(className == 'animals')
            animalCategories();
        else if(className == 'pictures')
            animalPictures();
    });
    
    $('div').on('click', '.category-class', function() {
        var currentId = $(this).data('id');
        showAnimalCategory(animals_data.category[$(this).data('id')]);
        
        $('div').on('click', '.my-category', function() {
           showAnimalDetails(animals_data.category[currentId].animals[$(this).data('id')]) 
        });
    });
})();







